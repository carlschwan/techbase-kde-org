Tutorials
=========

.. toctree::
   :titlesonly:
   :hidden:

   basic/index
   i18n/index

Basics of how to develop with KDE Frameworks
--------------------------------------------

Are you interested in writing applications with KDE Frameworks? This tutorial series is aimed at those completely new to it.

* :doc:`basic/setting_up`
  Before you start writing and building KDE software, you'll need to prepare your tools first.

* :doc:`basic/hello_world`

* :doc:`basic/main_window`

