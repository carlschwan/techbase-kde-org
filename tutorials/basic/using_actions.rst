Using Actions
=============

Summary
-------

This tutorial introduces the concept of actions. Actions are a unified way of supplying the user with ways to interact with your program.

For example, if we wanted to let the user of your :doc:`main_window` clear the text box by clicking a button in the toolbar, from an option in the File menu or through a keyboard shortcut, it could all be done with one `QAction <https://doc.qt.io/qt-5/qaction.html>`_. 


.. image:: using_actions/result.png
    :align: center


QAction
-------

A `QAction <https://doc.qt.io/qt-5/qaction.html>`_ is an object which contains all the information about the icon and shortcuts that is associated with a certain action. The action is then connected to a slot which carries out the work of your action. 

The Code
--------


