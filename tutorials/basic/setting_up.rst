Preparing your Development Environment
======================================

Contrary to popular misconception, you don't have to install all of "KDE" (you can't download a community) or a monolithic kdelibs (no longer exists) just to develop KDE software. That's especially true if you just want to use just a few `KDE Frameworks <https://kde.org/products/frameworks/>`_ for your Qt project. You don't even need to compile anything at all! Except for your own project, of course. Granted, there are a few ways to go about using KDE libraries for your application so this page lists some of them, depending on your situation or need. 

Installation
------------

Prerequisites
~~~~~~~~~~~~~

This guide focuses mainly on setting up KDE Frameworks. Whether you build it from source or use your distro's packages, you will need a few other pieces to use them.

* A `recent version of the Qt framework<https://community.kde.org/Frameworks/Policies#Frameworks_Qt_requirements>`.
* A recent version of CMake, but the KDE Frameworks also work with QMake
* Compiler - On Linux, GCC 4.8 and Clang 3.3 are required, enabling support for C++11 features

Easy as pie: Package manager
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The easiest and safest way to use a KDE framework is to install it (or its development package) from your Linux distribution's package manager. Say you want to add some syntax highlighting to your Qt text editing widget. On KDE Neon or Kubuntu or Debian, this one line sets it all up: 

.. code-block:: console

    sudo apt install libkf5syntaxhighlighting-dev

That's it! You can then proceed to actually :ref:`Using The Framework<using the framework>` in your app. Of course, this presumes that your distro has a more or less recent version of the framework in its repository or that you're fine with not using the latest. If not, proceed to Plan B.

Make it so!
~~~~~~~~~~~

KDE Frameworks is released on a monthly basis to ensure that developers will always have access to bug fixes or new features as soon as possible. The Plasma workspaces and KDE applications have their own release schedules as well. Sometimes, however, distros don't always provide the latest version of KDE software that you might need. If that's the case, you might have to compile the framework from the source. Not to worry, it's not as daunting or complicated as in the past.

Step 1: Download the framework
******************************

Download the latest version of the framework either from tarballs or from Git. Note that Git master will always have the latest version of the code and may not be stable, so be sure to checkout the stable tag for the release you need.


.. code-block:: console

    git clone https://anongit.kde.org/kwidgetsaddons.git

Step 2: Build it!
*****************

Go into the directory of the downloaded/cloned framework and issue the following commands:

.. code-block:: console

    mkdir build && cd build
    cmake .. -DCMAKE_INSTALL_PREFIX=$HOME
    make install

Presuming you have the proper dependencies installed (you'll have to consult your distro's package manager), this should end in a successful build that's ready to install. Note that KDE software use an "out-of-source build" structure to make things cleaner and easier to clean up if needed.

Step 3: Install
***************

This part is usually tricky. Normally, you'd issue the ``make install`` command at this point, which you can certainly do that if you already know the CMake trick. You can also use ``sudo make install`` but that would overwrite your system-installed version of the framework without prejudice. The safest way would be to install the framework :doc:`basic/using_actions<somewhere in your home directory>`, following the steps outlined in this tutorial. Do note you'll have to adjust some system environment variables so that your project will see the newly-installed framework. 

Use the source
~~~~~~~~~~~~~~

KDE does have a tool that specializes in automating the above. It might be overkill for one-off use of frameworks but if you find yourself getting attached to more and more KDE Frameworks (which is definitely a good thing), you might want to give the `kdesrc_build tool<http://community.kde.org/Get_Involved/development#Set_up_kdesrc-build>` a good try. It's especially useful for developers interested in leveling up to become KDE rockstars. 

Using KDE Frameworks
--------------------

Made it this far? Congratulations! The brunt of the setup work is complete and you're ready to use that powerful new framework to boost your Qt application. If you're using QMake, all you need is to add a simple line to your project file:

.. code-block:: qmake

    QT += KWidgetsAddons

If you're using CMake, be sure to check out the helpful Extra CMake Modules for making it easy to look for and add KDE Frameworks. Check out the Frameworks Quick Start Guide for a simple example of using the framework or jump right into the :doc:`basic<rest of Tutorials>`.

