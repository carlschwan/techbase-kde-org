Getting started
===============

.. toctree::
   :titlesonly:
   :hidden:

   setting_up
   hello_world
   main_window

* :doc:`setting_up`

* :doc:`hello_world`

* :doc:`main_window`

