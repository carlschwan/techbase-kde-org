## Installation

```bash
python3 -m venv venv
source venv/bin/activate
pip install sphinx_rtd_theme sphinx sphinxcontrib-doxylink aether-sphinx
```

## Build

```bash
make html
xdg-open _build/html/index.html
```
