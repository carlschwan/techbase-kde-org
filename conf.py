# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import aether_sphinx
import requests
from sphinx.util.console import bold

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'KDE Documentation'
copyright = '2019, KDE developers'
author = 'KDE developers'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'aether_sphinx',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

extensions = ['sphinxcontrib.doxylink']

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "aether"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# -- Processing

doxylink = {
    'kirigamiapi': ('Kirigami2.tags', 'https://api.kde.org/frameworks/kirigami/html/'),
    'kpeopleapi': ('KPeople.tags', 'https://api.kde.org/frameworks/kpeople/html/'),
    'kwidgetsaddonsapi': ('KWidgetsAddons.tags', 'https://api.kde.org/frameworks/kwidgetsaddons/html/'),
    'kcoreaddonsapi': ('KCoreAddons.tags', 'https://api.kde.org/frameworks/kcoreaddons/html/'),
    'kdeclarativeapi': ('KDeclarative.tags', 'https://api.kde.org/frameworks/kdeclarative/html/'),
    'ki18napi': ('KI18n.tags', 'https://api.kde.org/frameworks/ki18n/html/'),
    'kxmlguiapi': ('KXmlGui.tags', 'https://api.kde.org/frameworks/kxmlgui/html/'),
    'ktextwidgetsapi': ('KTextWidgets.tags', 'https://api.kde.org/frameworks/ktextwidgets/html/'),
}

for doc in doxylink.values():
    print(bold("Downloading file {} to {}".format(doc[1] + "/" + doc[0], doc[0])))
    tagFile = open(doc[0], "w")
    tagFile.write(requests.get(doc[1] + "/" + doc[0]).text)
    tagFile.close()
